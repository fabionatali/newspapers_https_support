from collections import defaultdict
import fileinput
import json
import math

import pycountry


NUMBER_OF_WEBSITES_THRESHOLD = 10


if __name__ == '__main__':
    """Generate https-support data for each country.

    ```
    $ cat input.jsonl
    {"url": "http://www.siasat.com/urdu", "country": "India", [...]}
    {"url": "http://www.huni.is/", "country": "Iceland", [...]}
    ...
    $ cat input.jsonl | python generate_data.py > output.json
    ```
    """

    total_websites_by_country = defaultdict(int)
    https_only_websites_by_country = defaultdict(int)
    https_compatible_websites_by_country = defaultdict(int)
    for line in fileinput.input():
        newspaper = json.loads(line)
        country = newspaper['country']
        is_https_only = newspaper['is_https_only']
        supports_https = newspaper['supports_https']

        if is_https_only not in (False, True):
            continue

        total_websites_by_country[country] += 1

        if is_https_only == True:
            https_only_websites_by_country[country] += 1
        else:
            https_only_websites_by_country[country] += 0

        if supports_https == True:
            https_compatible_websites_by_country[country] += 1
        else:
            https_compatible_websites_by_country[country] += 0

    data_by_country_code = []
    for country, total_websites in total_websites_by_country.items():
        https_compatible_websites = https_compatible_websites_by_country[country]
        https_support = '{0:0.2f}'.format(float(https_compatible_websites) / total_websites)

        https_only_websites = https_only_websites_by_country[country]
        https_only_support = '{0:0.2f}'.format(float(https_only_websites) / total_websites)

        try:
            country_code = pycountry.countries.get(name=country).alpha3
        except Exception as e:
            pass
        else:
            if total_websites > NUMBER_OF_WEBSITES_THRESHOLD:
                data_by_country_code.append({
                    "country": country_code,
                    "total_websites": total_websites,
                    "https_only_websites": https_only_websites,
                    "https_compatible_websites": https_compatible_websites,
                    "https_support": https_support,
                    "https_only_support": https_only_support,
                })
    print(json.dumps(data_by_country_code))
