import scrapy
import urlparse


class ForceUTF8Response(object):
    """A downloader middleware to force UTF-8 encoding for all responses."""

    encoding = 'utf-8'

    def process_response(self, request, response, spider):
        new_body = response.text.encode(self.encoding)
        return response.replace(body=new_body, encoding=self.encoding)


def fullname(o):
    return o.__module__ + "." + o.__name__


class NewspapersSpider(scrapy.Spider):
    """Crawls across the ABYZ website in search of newspaper websites.

    Mostly code targeting the ABYZ website and its peculiar
    structure. Aka shitty code ahead.
    """

    name = 'newspapers'
    allowed_domains = ['www.CONSIDER-USING-A-DIFFERENT-SOURCE.abyznewslinks.com']
    start_urls = ['http://www.CONSIDER-USING-A-DIFFERENT-SOURCE.abyznewslinks.com/allco.htm']
    custom_settings = {'SPIDER_MIDDLEWARES': {fullname(ForceUTF8Response): 100,}}

    def _extract_path_from_url(self, url):
        path = urlparse.urlparse(url).path.strip('/')
        if path:
            page_name = path.split('.')[0]
            return page_name

    def parse(self, response):
        for country in response.xpath('//div[7]/table/tr/td/font/a'):
            name = country.xpath('text()').extract_first()
            path = country.xpath('@href').extract_first()
            url = urlparse.urljoin(response.url, path.strip())
            request = scrapy.Request(url, self.parse_country_page)
            request.meta['country'] = name
            yield request

    def parse_country_page(self, response):
        links = response.xpath('//div/table/tr/td/font/a')
        internal_extractor = scrapy.linkextractors.LinkExtractor(
            allow_domains=u'abyznewslinks.com',
        )
        external_extractor = scrapy.linkextractors.LinkExtractor(
            deny_domains=u'abyznewslinks.com',
        )

        internal_links = internal_extractor.extract_links(response)
        external_links = external_extractor.extract_links(response)

        for external_link in external_links:
            yield {
                'country': response.meta['country'],
                'name': external_link.text,
                'url': external_link.url,
            }

        for internal_link in internal_links:
            internal_link_path = self._extract_path_from_url(internal_link.url)
            current_page_path = self._extract_path_from_url(response.url)
            if internal_link_path \
               and current_page_path \
               and internal_link_path != current_page_path \
               and internal_link_path.startswith(current_page_path):
                request = scrapy.Request(internal_link.url, self.parse_country_page)
                request.meta['country'] = response.meta['country']
                yield request
