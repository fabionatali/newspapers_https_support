import fileinput
import json
import requests
import urlparse


DEFAULT_TIMEOUT = 5


def is_https_only(url):
    """Whether a website is https-only."""

    # Force plain http
    url = urlparse.urlunsplit(urlparse.urlsplit(url)._replace(scheme='http'))
    # Check if http traffic is redirected to https
    try:
        page = requests.get(url, timeout=DEFAULT_TIMEOUT)
    except Exception as e:
        return e.__unicode__()
    if page.status_code != 200:
        return page.status_code
    return urlparse.urlsplit(page.url).scheme == 'https'


def supports_https(url):
    """Whether a website supports https."""

    # Force https
    url = urlparse.urlunsplit(urlparse.urlsplit(url)._replace(scheme='https'))
    # Check we get a response
    try:
        page = requests.get(url, timeout=DEFAULT_TIMEOUT)
    except Exception as e:
        return e.__unicode__()
    return page.status_code == 200


if __name__ == '__main__':
    """Check https-support of newspaper websites.

    Newspaper entries are read from the stdin and results are written
    to stdout. Each entry includes the newspaper name, url and
    country. The JSON Lines text file format is used, see
    http://jsonlines.org/.

    ```
    $ echo '{"url": "http://theguardian.com/", "country": "England", "name": "The Guardian"}' \
    > | python tutorial/spiders/newspapers_spider.py
    {"url": "http://theguardian.com/", "supports_https": true, "is_https_only": true, [...]}
    ```
    """

    for line in fileinput.input():
        newspaper = json.loads(line)
        url = newspaper['url']
        _is_https_only = is_https_only(url)
        _supports_https = _is_https_only or supports_https(url)
        newspaper['is_https_only'] = _is_https_only
        newspaper['supports_https'] = _supports_https
        print(json.dumps(newspaper))
