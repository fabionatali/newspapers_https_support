from newspapers_https_support.check_https_support import (
    is_https_only, supports_https
)
import pytest


def test_check_https_support():
    https_only_website = 'http://theguardian.com'
    https_website = 'http://example.com'
    non_https_website = 'http://reuters.com'

    assert supports_https(https_only_website)
    assert is_https_only(https_only_website)

    assert supports_https(https_website)
    assert not is_https_only(https_website)

    assert not supports_https(non_https_website)
    assert not is_https_only(non_https_website)
