# Newspapers Https Support

Newspapers Https Support is a proof-of-concept Python projects that
checks the https-support status across newspaper and media websites.

The software comes in two parts. The first part is a Scrapy bot that
collects website links. The second part is a script that, given a
certain website, checks its https-support.

## Usage

```
pip install -r requirements/base.txt
scrapy runspider scrapy_spider.py > newspapers.jsonl
cat newspapers.jsonl | python check_https_support.py > output.jsonl
```

## To do

The Scrapy crawler has been designed to work with
http://www.abyznewslinks.com/. This is a major limitation for at least
two reasons, lack of flexibility and the fact that the crawling can be
resource-consuming for the target website. This will be addressed in
an upcoming version of the software.
